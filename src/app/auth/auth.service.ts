import {
  BadRequestException,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { LoginDto } from './dto/login.dto';
import { InjectModel } from '@nestjs/mongoose';
import { User, UserDocument } from 'src/schemas/user.schema';
import { Model } from 'mongoose';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { UpdateProfileDto } from './dto/update-profile.dto';
import { ChangePasswordDto } from './dto/change-password.dto';

@Injectable()
export class AuthService {
  constructor(
    @InjectModel(User.schemaName)
    private readonly userModel: Model<UserDocument>,
    private jwtService: JwtService,
  ) {}

  async login(dto: LoginDto) {
    const users = await this.userModel.find({
      $or: [
        {
          username: dto.username,
        },
        {
          phone: dto.username,
        },
      ],
    });

    for (const user of users) {
      const isPasswordMatch = await bcrypt.compare(dto.password, user.password);

      if (isPasswordMatch) {
        // role used for RolesGuard
        const payload = { id: user._id, role: user.role };

        return {
          access_token: await this.jwtService.signAsync(payload),
        };
      }
    }

    throw new UnauthorizedException();
  }

  async getProfile(id: string) {
    return this.userModel.findById(id);
  }

  async updateProfile(id: string, dto: UpdateProfileDto) {
    return this.userModel.findByIdAndUpdate(
      id,
      { $set: { ...dto } },
      { new: true },
    );
  }

  async changePassword(id: string, dto: ChangePasswordDto) {
    const user = await this.userModel.findById(id);

    if (!user) {
      throw new NotFoundException('USER_NOT_FOUND');
    }

    const isPasswordMatch = await bcrypt.compare(
      dto.old_password,
      user.password,
    );

    if (!isPasswordMatch) {
      throw new BadRequestException('PASSWORD_NOT_MATCH');
    }

    const salt = await bcrypt.genSalt();
    user.password = await bcrypt.hash(dto.new_password, salt);
    return await user.save();
  }
}
