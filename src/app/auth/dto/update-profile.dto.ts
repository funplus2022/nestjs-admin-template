import { IsEmail, IsNumberString } from 'class-validator';

export class UpdateProfileDto {
  @IsEmail()
  email: string;

  @IsNumberString()
  phone: string;
}
