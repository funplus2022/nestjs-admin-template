import { IsEmail, IsEnum, IsNumberString, IsString } from 'class-validator';
import { UserRole } from 'src/schemas/user.schema';

export class UserDto {
  @IsString()
  username: string;

  @IsString()
  password: string;

  @IsEnum(UserRole)
  role: UserRole;

  @IsNumberString()
  phone: string;

  @IsEmail()
  email: string;

  created_at: string;
  updated_at: string;
}
