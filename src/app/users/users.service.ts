import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User, UserDocument } from 'src/schemas/user.schema';
import { UserDto } from './dto/user.dto';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User.schemaName)
    private readonly userModel: Model<UserDocument>,
  ) {}

  async create(dto: UserDto) {
    await this.passwordToHash(dto);
    const user = await this.userModel.create(dto);

    delete user.password;

    return user;
  }

  async findAll() {
    return this.userModel.find();
  }

  async findOne(id: string) {
    return this.userModel.findById(id);
  }

  async update(id: string, dto: UserDto) {
    await this.passwordToHash(dto);
    return this.userModel.findByIdAndUpdate(
      id,
      { $set: { ...dto } },
      { new: true },
    );
  }

  async remove(id: string) {
    return this.userModel.findByIdAndDelete(id);
  }

  private async passwordToHash(dto: UserDto) {
    const salt = await bcrypt.genSalt();
    dto.password = await bcrypt.hash(dto.password, salt);
  }
}
