import { SchemaOptions } from '@nestjs/mongoose';

export const schemaOptions: SchemaOptions = {
  virtuals: true,
  versionKey: false,
  toJSON: {
    transform(doc, ret) {
      ret.id = doc._id;

      delete ret._id;
      delete ret.password;

      return ret;
    },
  },
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  },
};
