import { Prop, Schema } from '@nestjs/mongoose';

@Schema()
export default class SchemaBase {
  static schemaName: string;

  @Prop()
  created_at: Date;

  @Prop()
  updated_at: Date;
}
