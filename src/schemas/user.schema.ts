import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import SchemaBase from './utils/schema-base';
import { HydratedDocument } from 'mongoose';
import { schemaOptions } from './utils/schema-options';

export enum UserRole {
  Admin = 'ADMIN',
  User = 'USER',
}

export type UserDocument = HydratedDocument<User>;

@Schema(schemaOptions)
export class User extends SchemaBase {
  static schemaName = 'users';

  @Prop()
  username: string;

  @Prop()
  password: string;

  @Prop()
  role: UserRole;

  @Prop()
  phone: string;

  @Prop()
  email: string;
}

export const UserSchema = SchemaFactory.createForClass(User);
