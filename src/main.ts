import { NestFactory } from '@nestjs/core';
import { AppModule } from './app/app.module';
import { Logger, ValidationPipe } from '@nestjs/common';
import helmet from 'helmet';
import * as compression from 'compression';

const port = parseInt(process.env.PORT) ?? 3000;

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.setGlobalPrefix('/api/v1');
  app.useGlobalPipes(new ValidationPipe());
  app.use(helmet());
  app.enableCors();
  app.use(compression());

  app.listen(port).then(() => {
    const logger = new Logger('SERVER');
    logger.log(`Server listening on ${port}`);
  });
}

bootstrap();
